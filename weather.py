# -*- coding: utf-8 -*-
import pandas
import numpy
import dateparser
import math
import seaborn
from matplotlib import pyplot
import matplotlib.ticker as ticker
from skcriteria import Data as critData, MIN, MAX
from skcriteria.madm import closeness

pandas.set_option('display.max_rows', 100)
pandas.set_option('display.max_columns', 20)
pandas.set_option('display.width', 1000)

class Weather:
	# Data: http://www.bom.gov.au/climate/dwo/IDCJDW0000.shtml
	
	def __init__(self, city):
		self.city = city
		self.columns = {
			'date': 'Date', 
			'day' :'Day', 	
			'temp_min' : 'Minimum temperature (°C)', 
			'temp_max' : 'Maximum temperature (°C)', 
			'temp_9am' : '9am Temperature (°C)', 
			'temp_3pm' : '3pm Temperature (°C)', 
			'wind_9am_from' : '9am wind direction', 
			'wind_9am_speed' : '9am wind speed (km/h)', 
			'wind_3pm_from' : '3pm wind direction', 
			'wind_3pm_speed' : '3pm wind speed (km/h)', 
			'wind_gustMax_from' : 'Direction of maximum wind gust ', 
			'wind_gustMax_speed' : 'Speed of maximum wind gust (km/h)', 
			'wind_gustMax_time' : 'Time of maximum wind gust', 
			'clouds_9am' : '9am cloud amount (oktas)', 
			'clouds_3pm' : '3pm cloud amount (oktas)', 
			'pressure_9am' : '9am MSL pressure (hPa)', 
			'pressure_3pm' : '3pm MSL pressure (hPa)',
			'humidity_9am' : '9am relative humidity (%)', 
			'humidity_3pm' : '3pm relative humidity (%)', 
			'rain' :'Rainfall (mm)', 
			'evaporation' :'Evaporation (mm)', 
			'sunshine' : 'Sunshine (hours)'}
		self.columns_inverse = dict(map(reversed, self.columns.items()))
		self.load()
		self.clean()
		self.transform()
	
	def load(self):
		self.data_raw = pandas.read_csv('data/{0}.csv'.format(self.city))	

	def clean(self):
		df = self.data_raw.copy()
		df.rename(columns=self.columns_inverse, inplace=True)
		# remove unnamed columns
		df.loc[:, ~df.columns.str.contains('^Unnamed')]
		
		# uniform date and time format and tranform to datetime object
		df['date'] = df.date.apply(lambda x: dateparser.parse(x, date_formats=['%d/%m/%Y'])) 
		
		# wind speed fill NaN values if calm wind
		df['wind_9am_speed'] = df.apply(lambda x: 0 if x.wind_9am_from == 'Calm' else x.wind_9am_speed, axis=1)
		df['wind_3pm_speed'] = df.apply(lambda x: 0 if x.wind_3pm_from == 'Calm' else x.wind_3pm_speed, axis=1)
		
		# clouds percentage
		df['clouds_9am'] = df.clouds_9am.apply(lambda x: 1 if x == 9 else x/8)
		df['clouds_3pm'] = df.clouds_3pm.apply(lambda x: 1 if x == 9 else x/8)
		
		# check for unexpected data format
		df = df[['temp_min', 'temp_max', 'temp_9am', 'temp_3pm', 'wind_9am_speed', 'wind_3pm_speed', 'rain', 'sunshine', 'humidity_9am', 'humidity_3pm', 'clouds_9am', 'clouds_3pm']]
		df.apply(pandas.to_numeric) #will raise error if not numeric
		
		# only temp can be negative
		check_columns = ['wind_9am_speed', 'wind_3pm_speed', 'rain', 'sunshine', 'humidity_9am', 'humidity_3pm', 'clouds_9am', 'clouds_3pm']
		for column in check_columns:
			df[column] = df[column].apply(lambda x: x if x>=0 else numpy.nan)

		# remove outliers (1.5*IQR)
		Q1 = df.quantile(0.25)
		Q3 = df.quantile(0.75)
		IQR = Q3-Q1
		outlierDomain = (df < (Q1 - 1.5 * IQR)) | (df > (Q3 + 1.5 * IQR))
		df = df[~outlierDomain]
		
		self.df_clean = df

	def temp_dewpoint(self, temp, humidity):
		a = 17.27
		b = 237.7
		def gamma(temp,humidity):
			return (a * temp / (b + temp)) + numpy.log(humidity/100.0)
		return (b * gamma(temp,humidity)) / (a - gamma(temp,humidity))

	def temp_apparent(self, temp_max_mean, humidity_3pm_mean, wind_speed_mean):
		e = (humidity_3pm_mean / 100) * 6.105 * math.exp( 17.27 * temp_max_mean / (237.7 + temp_max_mean))
		return temp_max_mean + 0.33*e - 0.7*wind_speed_mean - 4	

	def temp_comfort_index(self, temp):
		return -0.02124*(temp**2) + 0.7432*temp + 15.93

	def humidity_comfort_index(self, temp_dewpoint):
		return 1.677087 + 0.7791388*temp_dewpoint + 0.01930845*(temp_dewpoint**2) - 0.001595287*(temp_dewpoint**3)

	def transform(self):
		df = self.df_clean.copy()
		df['city'] = self.city
		df['temp_mean'] = df.apply(lambda x:  numpy.mean([x.temp_min, x.temp_max, x.temp_9am, x.temp_3pm]), axis=1)
		df['wind_speed_mean'] = df.apply(lambda x:  numpy.mean([x.wind_9am_speed, x.wind_3pm_speed]), axis=1)
		df['humidity_mean'] = df.apply(lambda x:  numpy.mean([x.humidity_9am, x.humidity_3pm]), axis=1)
		df['temp_apparent_mean'] = df.apply(lambda x:  self.temp_apparent(x.temp_max, x.humidity_3pm, x.wind_speed_mean), axis=1)
		df['temp_dewpoint_mean'] = df.apply(lambda x:  self.temp_dewpoint(x.temp_mean, x.humidity_mean), axis=1)
		df['temp_comfort_index'] = df.temp_mean.apply(lambda x: self.temp_comfort_index(x))
		df['humidity_comfort_index'] = df.temp_dewpoint_mean.apply(lambda x: self.humidity_comfort_index(x))
		self.df_transformed = df

	def describe(self, df):
		print ""
		print "{0} Data: ".format(self.city)
		print "---------------------------------"
		print df
		print "{0} Desciption: ".format(self.city)
		print "---------------------------------"
		print df.info()
		print "{0} Availability: ".format(self.city)
		print "---------------------------------"
		print (df.count() / df.shape[0]) * 100

	def violinplot(self, columns, style = "whitegrid"):
		seaborn.set_style(style)
		data = self.df_transformed[columns].copy()
		
		transposed = data.stack().reset_index()
		transposed.columns = 'row indicator value'.split()
		transposed = transposed.sort_values(by=['indicator']).reset_index(drop=True)
		
		fig, ax = pyplot.subplots(figsize =(16, 10))
		ax.yaxis.set_major_locator(ticker.MultipleLocator(5))
		seaborn.violinplot(ax = ax, x='indicator', y='value', data=transposed) 
		fig.savefig("plots/{0}.svg".format(self.city))




if __name__ == "__main__":
	
	cities = ["Abelaide", "Brisbane", "Melbourne", "Perth", "Sydney"]
	
	# Load
	store = {city: Weather(city) for city in cities}
	data = {city: store[city].df_transformed for city in cities}
	dataAll = pandas.concat([data[city] for city in cities]) # union all cities
	
	# Plot
	interestingItems = ['temp_mean', 'temp_dewpoint_mean', 'temp_apparent_mean', 'sunshine', 'wind_speed_mean', 'rain']

	# Plot -- Grouped by city
	def plot(data, column, style = "whitegrid"):
		seaborn.set_style(style)
		fig, ax = pyplot.subplots(figsize =(16, 10))
		# ax.yaxis.set_major_locator(ticker.MultipleLocator(5))
		seaborn.violinplot(ax = ax, x='city', y=column, data=data) 
		fig.savefig("plots/compare_{0}.png".format(column))	
	for item in interestingItems:
		plot(dataAll, item, "whitegrid")
	

	# Solve
	criteria_names = ['temp_comfort_index', 'humidity_comfort_index', 'wind_speed_mean', 'sunshine']
	criteria_goals = [MAX, MAX, MIN, MAX]
	
	dataAll_subset = dataAll[criteria_names]
	dataAll_median = dataAll.groupby(dataAll.city)[criteria_names].median()
	dataAll_median = dataAll_median.round(0).astype(int)
	
	data_ = critData(dataAll_median, criteria_goals, anames=dataAll_median.index.tolist(), cnames=criteria_names)
	
	decision_maker = closeness.TOPSIS()
	ranking = decision_maker.decide(data_)
	print ranking
	